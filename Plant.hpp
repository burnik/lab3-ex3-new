#ifndef PLANT_H
#define PLANT_H

#include <iostream>	//Для перегрузки "<<" в файле реализации
#include <string.h>
#include <iomanip>

#define NAME_MAX_SIZE 32

using namespace std;

struct Plant
{
	char name[NAME_MAX_SIZE];	//Название растения
	short month;				//Месяц посадки

	unsigned int price,		//Цена за упаковку
				 amount;	//Количество семян в упаковке


	friend ostream& operator<<(ostream& os, const Plant& dt)
	{
		cout << "Название: " << setw(16) << dt.name << "; Месяц: " << setw(3) << dt.month
		     << "; Цена: " << setw(5) << dt.price << "; Кол-во: " << setw(3) << dt.amount;
		return os;
	};

};
#endif
