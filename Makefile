all: l3_e4

WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O0
CPP_VERSION = c++20
CC = g++

SOURCES = main.cpp TUIlist.hpp TUImenu.hpp Plant.hpp table.hpp


l3_e4: $(SOURCES)
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) -J7 -std=$(CPP_VERSION) $(SOURCES)

clean:
	rm -f l3_e4

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	./l3_e4
