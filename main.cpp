#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

#include "TUImenu.hpp"
#include "TUIlist.hpp"
#include "Plant.hpp"

using namespace std;

//Функция чтения из файла
void showArray(vector<Plant> *arr)
{
    int buf;
    cout << "0) Назад" << endl
         << "1) Вывести массив в исходном порядке" << endl
         << "2) Вывести массив в обратном порядке" << endl
         << "> ";
    cin >> buf;

    if (!buf)
        return;

    if (buf == 1)
        TUIlist(*arr);
    else if (buf == 2)
    {
        reverse(arr->begin(), arr->end());
        TUIlist(*arr);
        reverse(arr->begin(), arr->end());
    }
    else
        cout << "Некорректный ввод" << endl;

}

//Функция чтения из файла
void readFromFile(vector<Plant> *arr)
{
    //Вводим название файла
    string fileName;
    cout << "Введите имя файла> ";
    cin >> fileName;

    //Открываем файл для чтения
    ifstream input(fileName, ios::binary);

    //Произошла ошибка при открытии файла, возможно, он не существует.
    if (input.fail())
    {
        cout << "Ошибка при открытии файла, возможно, он не существует" << endl
             << "Текущий массив не будет изменён" << endl;
        return;
    }

    //Удаляем данные из массива
    arr->clear();

    //Читаем данные из файла пока это возможно
    Plant buffer;
    while (input.read((char *)&buffer, sizeof(Plant)))
    {
        if (!input.good())
        {
            string bufStr = "";
            cout << "Произошла ошибка при чтении. Возможно, файл повреждён." << endl
                 << "Продолжить чтение? [y/N]>";
            cin >> bufStr;

            if (tolower(bufStr[0]) == 'y')
                continue;

            break;
        }

        arr->push_back(buffer);
    }

    //Закрываем файл
    input.close();
}

void writeToFile(vector<Plant> *arr)
{
    string fileName;
    cout << "Введите название файла> ";
    cin >> fileName;

    ofstream fout(fileName, ios::out | ios::binary);

    fout.write((char*)&arr->at(0), arr->size() * sizeof(Plant));
    fout.close();
}

void addElement(vector<Plant> *arr)
{
    Plant newPlant;

    cout << "Название растения> ";
    cin >> newPlant.name;

    cout << "Месяц посадки [1-12]> ";
    cin >> newPlant.month;

    cout << "Цена растения> ";
    cin >> newPlant.price;

    cout << "Количество семян в упаковке> ";
    cin >> newPlant.amount;

    arr->push_back(newPlant);
}

void printFilterByMonth(vector<Plant> *arr)
{
    vector<Plant> newVector;

    copy_if(arr->begin(), arr->end(),
             back_inserter(newVector),
             [](Plant pl) { return pl.month >= 3 && pl.month <= 5; });

    TUIlist(newVector);
}

void changePriceByName(vector<Plant> *arr)
{
    char name[NAME_MAX_SIZE];
    cout << "Введите имя растения, цену для которого нужно изменить> ";
    cin >> name;

    short price;
    cout << "Введите новую цену> ";
    cin >> price;

    for (size_t i = 0; i < arr->size(); i++)
        if (strcmp(arr->at(i).name, name) == 0)
            arr->at(i).price = price;
}

int main()
{
    vector<Plant> arr;

    //Задаём меню
    TUImenu::arg = &arr;

    //Задаём элементы
    TUImenu::addElement(readFromFile,       "Прочитать из файла");
    TUImenu::addElement(writeToFile,        "Записать в файл");
    TUImenu::addElement(addElement,         "Добавить элемент в массив");
    TUImenu::addElement(showArray,          "Вывести массив");
    TUImenu::addElement(printFilterByMonth, "Семена которые можно высаживать с марта по май");
    TUImenu::addElement(changePriceByName,  "Изменить цену семян по его названию");
    TUImenu::start();


    return 0;
}
